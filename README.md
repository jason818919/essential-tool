# **Introduction**
------
Automatically install the essential tools for server.

# **Usage**
------
```
$ ./install
```

# **Author**

Jason Chen
